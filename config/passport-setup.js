const passport=require('passport')
const GoogleStrategy=require('passport-google-oauth20')
const keys=require('./keys')
var https=require('https')
function extractProfile(profile) {
  let imageUrl = '';
  if (profile.photos && profile.photos.length) {
    imageUrl = profile.photos[0].value;
  }
  return {
    id: profile.id,
    displayName: profile.displayName,
    image: imageUrl,
  };
}
passport.use(new GoogleStrategy({
  //options for GoogleStrategy
  callbackURL:'/auth/google/redirect',
   clientID:keys.google.clientID,
   clientSecret:keys.google.clientSecret
},(accesstoken ,refreshtoken, profile,cb)=>{
  //passport callback function
  console.log(profile.displayName)
  console.log("accesstoken"+accesstoken)
  var options = {
                   hostname: 'www.googleapis.com',
                   port: 443,
                   path: '/userinfo/v2/me',
                   method: 'GET',
                   json: true,
                   headers:{
                       Authorization: 'Bearer ' + accesstoken
                  }
           };
   console.log(options);
   var resObj
   var getReq = https.request(options, function(res) {
       console.log("\nstatus code: ", res.statusCode);
       res.on('data', function(response) {
           try {
                resObj = JSON.parse(response);
               console.log("response: ", resObj);
           } catch (err) {
               console.log(err);
           }
       });
   });
   getReq.end();
   getReq.on('error', function(err) {
       console.log(err);
   });
  cb(null, extractProfile(profile));
})
)
passport.serializeUser((user, cb) => {
  cb(null, user);
});
passport.deserializeUser((obj, cb) => {
  cb(null, obj);
});
