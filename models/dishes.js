const mongoose=require('mongoose')
require('mongoose-currency').loadType(mongoose);
const currency=mongoose.Types.Currency;
const schema=mongoose.Schema;
const commentSchema=new schema({
  rating:{
  type:Number,
  min:1,
  max:5,
  require:true
   },
  comment:{
    type:String,
    required:true
  },
  author:{
    type:mongoose.Schema.Types.ObjectId,
    ref:'user',

  }
},{
   timestamps:true
})
const dishSchema=new schema({
  name:{
    type:String,
    unique:true,
    required:true
  },
  description:{
    type:String,
    required:true
  },
  image:{
    type:String,
    require:true
  },
  category:{
    type:String,
    require:true
  },
  label:{
    type:String,
    default:''
  },
  price:{
    type:currency,
    require:true,
    min:0
  },
  comments:[commentSchema],
  featured:{
    type:Boolean,
    default:false
  },
},{
    timestamps:true
});
const dish2=new schema({
  name:{
    type:String,
    unique:true,
    required:true
  },
  description:{
    type:String,
    required:true
  }
})
var Dishes=mongoose.model('dish1',dishSchema);
var Dish2=mongoogse.model('dish2',dish2);
module.exports=Dishes
module.exports=Dish2
